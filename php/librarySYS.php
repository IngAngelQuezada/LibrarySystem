<?php
$Name = $_GET['name'];
$decode = base64_decode($Name);
$upper = strtoupper($decode);
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/principal_styles.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/fontAwesome5.0.13/web-fonts-with-css/css/fontawesome-all.css">
	<script src="../js/jquery-3.3.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/principal.js"></script>
	<title>MAIN PAGE | User: <?php echo $upper ?></title>
</head>
<body style="background-color: darkgray;">
	<div class="jumbotron">
		<center><h2 id="title">Library Center</h2></center>
		<center><h3><small class="text-muted">Where the books are good!</small></h3></center>
		<?php
		echo "<center><h4>Welcome: " .$decode. "!</h4></center>";
		?>
		<button input type="button" class="btn btn-default" onclick="manage_pictures()" id="logPicture">
			Manage Pictures.</button>
			<button input type="button" class="btn btn-primary" onclick="main_page()">
				Return to Main Page</button>
			</div>
			<center><h1>Choose Your Option Below:</h1></center>
			<div class="row">

				<div class="col-sm-4">
					<center><figure class="figure" id="tableBook">
						<img src="../src/buy.png" width="200" height="200" class="figure-img img-fluid rounded" alt="BUY A BOOK">
						<center><h4><figcaption class="figure-caption" id="textBook">Look all the books that we have</figcaption></h4></center>
					</figure></center>
					<div class="clearfix"></div>
					<center><input type="button" class="btn btn-primary" id="buyBook" value="Buy a Book" onclick="buyModal()"></button></center>
				</div>

				<div class="col-sm-4">
					<center><figure class="figure" id="searchBook">
						<img src="../src/search.png" width="200" height="200" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
						<center><h4><figcaption class="figure-caption" id="textSearchBook">Search a Book</figcaption></center></h4>
					</figure></center>
					<div class="clearfix"></div>
					<center><button type="button" class="btn btn-success">Search</button></center>
				</div>

				<div class="col-sm-4">
					<center><figure class="figure" id="aboutBook">
						<img src="../src/about.png" width="200" height="200" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
						<center><h4><figcaption class="figure-caption" id="textAboutBook">Read About our Library History</figcaption></center></h4>
					</figure></center>
					<div class="clearfix"></div>
					<center><button type="button" class="btn btn-info">About</button></center>
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<!-- BUY MODAL-->
			<div class="modal fade" id="buyModal" role="dialog">
				<div class="modal-dialog modal-xl">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Buy a Book</h4>
						</div>
						<div class="modal-body">
							<form action="shopping/buy.php" method="POST">
								<h3>Modal in Progress...</h3>
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-success" value="Continue">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		
			<footer class="page-footer font-small unique-color-dark pt-3" id="myFooter">
				<div style="background-color: #6351ce;">
					<div class="container">
					</div>
					<div class="footer-copyright py-3 text-center">
						© 2018 Copyright:
						<strong>|Miguel Angel Quezada Galván|</strong>
					</a>
				</div>
		</footer>
	</body>
	</html>
